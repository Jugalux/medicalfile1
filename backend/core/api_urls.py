from django.urls import path
from rest_framework.routers import DefaultRouter

from core.api_views import DossierMedicalAPIView, ObtenirDossierMedicalAPIView


router = DefaultRouter()


urlpatterns = [
    path('medical_folder/', DossierMedicalAPIView.as_view()),
    path('get_medical_folder/', ObtenirDossierMedicalAPIView.as_view()),
]

urlpatterns += router.urls
