from random import randint

STRINGS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
NUMBERS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']


def generate_code():
    """Génère un code unique pour le dossier médical."""
    
    # Liste qui contient les différentes chaines qui constitueront le code
    iterable = []
    for i in range(5):
        # Chaine qui contient les caractères qui constitueront la chaine
        seed = ''
        for j in range(5):
            if randint(0,1):
                seed += STRINGS[randint(0, len(STRINGS) - 1)]
            else:
                seed += NUMBERS[randint(0, len(NUMBERS) - 1)]
        iterable.append(seed)
    
    return '-'.join(iterable)