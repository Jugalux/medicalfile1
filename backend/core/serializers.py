from rest_framework import serializers

from core.models import DossierMedical


class DossierMedicalSerializer(serializers.ModelSerializer):
    code = serializers.ReadOnlyField()
    date_creation = serializers.ReadOnlyField()
    
    class Meta:
        model = DossierMedical
        fields = '__all__'


class ObtenirDossierMedicalSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=100)

    def validate(self, attrs):
        code = attrs.get('code')
        if len(code) != 25:
            attrs['error'] = "Le code doit contenir 25 caractères."
        
        if len(code.split('-')) != 5:
            attrs['error'] = "Le code n'est pas valide."
            
        return attrs
