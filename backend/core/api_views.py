from rest_framework.response import Response
from rest_framework import generics
from core.models import DossierMedical

from core.serializers import DossierMedicalSerializer, ObtenirDossierMedicalSerializer
from core.utils import generate_code


class DossierMedicalAPIView(generics.GenericAPIView):
    serializer_class = DossierMedicalSerializer
    queryset = DossierMedical.objects.all()
    
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        code = ''
        while True:
            code = generate_code()
            if DossierMedical.objects.filter(code=code).exists():
                code = generate_code()
                
            else:
                break
        
        dossier = DossierMedical.objects.create(
            code=code,
            nom=serializer.validated_data['nom'],
            prenom=serializer.validated_data['prenom'],
            date_de_naissance=serializer.validated_data['date_de_naissance'],
            lieu_de_naissance=serializer.validated_data['lieu_de_naissance'],
            poids=serializer.validated_data['poids'],
            taille=serializer.validated_data['taille'],
            sexe=serializer.validated_data['sexe'],
            profession=serializer.validated_data['profession'],
            numero_cni=serializer.validated_data['numero_cni'],
            telephone=serializer.validated_data['telephone'],
            status=serializer.validated_data['status']
        )
        
        return Response(dossier)
    
    def get(self, request):
        queryset = self.get_queryset().filter(status='Publique')
        serializer = self.get_serializer(queryset, many=True)
        
        return Response(serializer.data)
    

class ObtenirDossierMedicalAPIView(generics.GenericAPIView):
    serializer_class = ObtenirDossierMedicalSerializer
    
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        if serializer.validated_data.get('error'):
            return Response(serializer.validated_data['error'])
        
        dossier = DossierMedical.objects.get(code=serializer.validated_data['code'])
        if dossier:
            return Response(dossier)
        
        else:
            return Response('Dossier non trouvé')
