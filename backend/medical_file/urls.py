from django.contrib import admin
from django.urls import include, path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns     # <--- Ajouter cette ligne pour déployer sur Render

urlpatterns = [
    path('admin/', admin.site.urls),
    path('medical-folder/api/', include('medical_file.api_urls')),
]

urlpatterns += staticfiles_urlpatterns()        # <--- Ajouter cette ligne pour déployer sur Render
