import React from 'react'
import Navbar from '../../components/pagesSections/Navbar'
import Footer from '../../components/pagesSections/Footer'
import ContainerMedicalFolder from './container/ContainerMedicalFolder'

const MedicalFolder = () => {
  return (
    <section className="w-full min-h-[100vh]">
      <Navbar page='file' />
        <ContainerMedicalFolder/>
      <Footer />
    </section>
  )
}

export default MedicalFolder