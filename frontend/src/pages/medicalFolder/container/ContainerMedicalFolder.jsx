import React from "react";
import { FaDownload } from "react-icons/fa";
import { useSearchParams } from "react-router-dom";
import Button from "../../../components/elements/Button";
import SectionBox from "../../../components/pagesSections/medicalFilePage/SectionBox";

const data = {
  nom:"NCHOUWET MFOUAPON",
  prenom:"Kuntz Stephane",
  profession:"Etudiant",
  date:"2023-01-11",
  lieu:"baleng",
  tel:"655 30 43 74",
  taille:1.87,
  poids:75,
  sexe:"masculin",
  cni:"DD556DE98F",
  statut:"privé",
  observations:[
    {
      content:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus rutrum urna, sit amet vehicula lacus vulputate vitae. Nulla facilisi. Cras iaculis ex ac metus semper, vel commodo felis volutpat. Proin maximus quam at bibendum sollicitudin. Ut mi libero, aliquam eget nisi lacinia, efficitur interdum diam. Donec eleifend urna lectus, non consequat eros suscipit sit amet. Ut laoreet libero vel ex sollicitudin molestie. Cras vulputate eget mi a tristique. Aenean in justo et diam mattis scelerisque. Phasellus vulputate, purus ut venenatis sagittis, ex eros laoreet nibh, ut tincidunt urna massa eget tortor. Etiam fermentum purus venenatis, vulputate est vel, iaculis elit. Nulla sagittis orci eu ultricies molestie. Cras eget gravida odio.",
      date:"2023-01-11"
    }
  ],
  
  exams:[
    {
      content:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus rutrum urna, sit amet vehicula lacus vulputate vitae. Nulla facilisi. Cras iaculis ex ac metus semper, vel commodo felis volutpat. Proin maximus quam at bibendum sollicitudin. Ut mi libero, aliquam eget nisi lacinia, efficitur interdum diam. Donec eleifend urna lectus, non consequat eros suscipit sit amet. Ut laoreet libero vel ex sollicitudin molestie. ",
      date:"2023-01-11"
    }
  ],
  ordonnances:[
    {
      content:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus rutrum urna, sit amet vehicula lacus vulputate vitae. Nulla facilisi. Cras iaculis ex ac metus semper, vel commodo felis volutpat. Proin maximus quam at bibendum sollicitudin. Ut mi libero, aliquam eget nisi lacinia, efficitur interdum diam. Donec eleifend urna lectus, non consequat eros suscipit sit amet. Ut laoreet libero vel ex sollicitudin molestie. Cras vulputate eget mi a tristique. Aenean in justo et diam mattis scelerisque. Phasellus vulputate, purus ut venenatis sagittis, ex eros laoreet nibh, ut tincidunt urna massa eget tortor. Etiam fermentum purus venenatis, vulputate est vel, iaculis elit. Nulla sagittis orci eu ultricies molestie. Cras eget gravida odio.",
      date:"2023-01-11"
    }
  ]
}

const isPatient = false;

const ContainerMedicalFolder = () => {

  const [searchParams] = useSearchParams()
  console.log(searchParams.get('id'))

  const handleDownload = () => {
    console.log("donwload");
  }

  return (
    <div className="min-h-[100vh] max-w-full p-5">
      {/** bouton télécharger */}
      <div className=" flex justify-end">
        <div className="w-[130px] md:w-[160px] flex-wrap">
          <Button
            filled={false}
            name="Télécharger"
            handleClick={handleDownload}
          >
            <FaDownload />
          </Button>
        </div>
      </div>

     <SectionBox data = {data} isPatient ={isPatient}/>
    </div>
  );
};

export default ContainerMedicalFolder;
