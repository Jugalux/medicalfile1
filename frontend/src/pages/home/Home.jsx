import React from 'react'
import Navbar from '../../components/pagesSections/Navbar'
import ContainerHome from './container/ContainerHome'

function Home() {

    return (
        <div className=''>
            <Navbar page='home' />
            <ContainerHome />
        </div>
    )
}

export default Home