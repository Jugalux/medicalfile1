import React, { useState } from 'react'
import { AiFillFolder } from 'react-icons/ai'
import { Modal } from "@mui/material"
import ModalAddFile from '../../../components/pagesSections/ModalAddFile'
import banner from '../../../media/banner.png'


function ContainerHome() {

    const [openModal, setOpenModal] = useState(false)
    const handleClose = () => setOpenModal(false)
	const handleOpen = () => setOpenModal(true)

    return (
        <div className='grid grid-cols-5 p-10 pb-0'>
            <div className='col-span-3 lg:pt-16'>
                <h1 className='text-4xl font-bold my-5'>
                    Plateforme de gestion de  dossier medicaux partagés
                </h1>
                <p>
                    Toutes vos données sont privées et confidentielles mais pour des besoins
                     de consultations ou d'assistance, vous avez la possibilité de partager votre 
                     dossier avec un médecin (écriture) ou avec un proche (lecture ou écriture).
                </p>
                <div className='flex mt-12'>
                    <div onClick={() => handleOpen()} className="flex lg:px-10 px-4 lg:py-3 py-2 bg-primary text-white border rounded-full hover:cursor-pointer">
                        <AiFillFolder color='white' size={30} className='mr-2' />
                        <p className='text-lg'>Accéder à mon dossier</p>
                    </div>
                    <Modal
                        open={openModal}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                        sx={{ overflowY: "scroll" }}
                    >
                        <div className="w-full">
                            <ModalAddFile close={handleClose} />
                        </div>
                    </Modal>
                </div>
            </div>
            <div className='col-span-2'>
                <img src={banner} alt="" />
            </div>
        </div>
    )
}

export default ContainerHome