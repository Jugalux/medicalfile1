import React from 'react'
import ContainerSearch from './container/ContainerSearch'
import Navbar from '../../components/pagesSections/Navbar'
import Footer from '../../components/pagesSections/Footer'

const Search = () => {
    return (
        <section>
            <Navbar page='search' />
                <ContainerSearch/>
            <Footer />
        </section>
    )
}

export default Search