import React, { useState, useEffect } from "react"
import axios from '../../../components/api/axios'
import Title from "../../../components/elements/Title"
import SearchBar from "../../../components/elements/SearchBar"
import UserBox from "../../../components/pagesSections/searchPage/UserBox"

const ContainerSearch = () => {

	const [searchQuery, setSearchQuery] = useState("")
	const [files, setFiles] = useState([])

	useEffect(() => {
		getData()
	})

	const getData = async() => {
		const response = await axios.get('/medical_folder')
		setFiles(response.data)
	}

	const filterData = (query, data) => {
		if (!query) {
		  return data;
		} else {
		  return data.filter((d) => ( d.code.includes(query) || d.nom.toLowerCase().includes(query) || d.prenom.toLowerCase().includes(query) ));
		}
	}
	const dataFiltered = filterData(searchQuery, files)

	return (
		<div className="w-full min-h-[100vh] lg:p-10 md:p-10 p-3 mb-10">

			<div className='flex justify-center py-7'>
				<Title titleName='Dossier Medical partagé' subtitleName='Recherchez des patient par leurs noms ou leur code d’identification' />
			</div>
			<div className='mb-16'>
				<SearchBar searchQuery={searchQuery} setSearchQuery={setSearchQuery} />
			</div>

			<div className='grid lg:grid-cols-5 md:grid-cols-3 gap-4'>
				{dataFiltered.map((file) => (
					<div
						key={file.id}
					>
						<UserBox name={file.nom} surname={file.prenom} sex={file.sexe} id={file.id} />
					</div>
				))}
			</div>

		</div>
	)
}

export default ContainerSearch;
