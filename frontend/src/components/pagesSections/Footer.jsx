import React from 'react'

function Footer() {

    return (
        <div className='w-full bg-primary py-3'>
            <p className='text-center text-gray-300'>copyright@2023 - INF204027 - Groupe2</p>
        </div>
    )
}

export default Footer