import React, { useState } from "react";
import Input from "./../elements/Input";
import SelectInput from "./../elements/SelectInput";

const sexeEnum = ["Masculin", "Féminin"];
const statutEnum = ["Public", "Privé"];

const PersonalInfoInput = ({ disabled, itemsPerLine, modify, data }) => {
  const [formInputs, setFormInputs] = useState({
    nom: !modify ? "" : data.nom,
    prenom: !modify ? "" : data.prenom,
    date: !modify ? "" : data.date,
    lieu: !modify ? "" : data.lieu,
    taille: !modify ? "" : data.taille,
    poids: !modify ? "" : data.poids,
    sexe: !modify ? sexeEnum[0] : data.sexe,
    profession: !modify ? "" : data.profession,
    cni: !modify ? "" : data.cni,
    tel: !modify ? "" : data.tel,
    statut: !modify ? statutEnum[0] : data.statut,
  });

  const inputsList = [
    {
      name: "Nom",
      varName: "nom",
      value: formInputs.nom,
      type: "text",
      items: null,
    },
    {
      name: "Prénom",
      varName: "prenom",
      value: formInputs.prenom,
      type: "text",
      items: null,
    },
    {
      name: "Date de naissance",
      varName: "date",
      value: formInputs.date,
      type: "date",
      items: null,
    },
    {
      name: "Lieu de naissance",
      varName: "lieu",
      value: formInputs.lieu,
      type: "text",
      items: null,
    },
    {
      name: "Taille (mètres)",
      varName: "taille",
      value: formInputs.taille,
      type: "text",
      items: null,
    },
    {
      name: "Poids (kg)",
      varName: "poids",
      value: formInputs.poids,
      type: "number",
      items: null,
    },
    {
      name: "Sexe",
      varName: "sexe",
      value: formInputs.sexe,
      type: "select",
      items: sexeEnum,
    },
    {
      name: "Profession",
      varName: "profession",
      value: formInputs.profession,
      type: "text",
      items: null,
    },
    {
      name: "Numéro de CNI",
      varName: "cni",
      value: formInputs.cni,
      type: "text",
      items: null,
    },
    {
      name: "Numéro de télephone",
      varName: "tel",
      value: formInputs.tel,
      type: "text",
      items: null,
    },
    {
      name: "Statut",
      varName: "statut",
      value: formInputs.statut,
      type: "select",
      items: statutEnum,
    },
  ];

  const handleChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;
    setFormInputs({ ...formInputs, [name]: value });
  };

  const handleDisable = (name) => {
    if (!modify) {
      return disabled;
    } 
    else {
      if (disabled) 
      {
        return disabled;
      } 
      else {
        if (
          name === "nom" ||
          name === "prenom" ||
          name === "date" ||
          name === "lieu" ||
          name === "sexe"
        ) {
          return true;
        } else {
          return false;
        }
      }
    }
  };

  return (
    <section className="">
      <div className="flex flex-wrap justify-center items-center">
        {inputsList.map((input, id) => {
          return (
            <div
              key={id}
              className={` w-full ${
                itemsPerLine === 2 ? " md:w-[47%]" : " md:w-[47%] lg:w-[32%]"
              } md:mr-3 my-2`}
            >
              {input.type !== "select" && (
                <Input
                  type={input.type}
                  name={input.name}
                  varName={input.varName}
                  value={input.value}
                  handleChange={handleChange}
                  disabled={handleDisable(input.varName)}
                />
              )}

              {input.type === "select" && (
                <SelectInput
                  name={input.name}
                  varName={input.varName}
                  selectedValue={input.value}
                  handleChange={handleChange}
                  listValues={input.items}
                  disabled={handleDisable(input.varName)}
                />
              )}
            </div>
          );
        })}
      </div>
    </section>
  );
};

export default PersonalInfoInput;
