import React, { useState } from "react"
import { ImCross } from "react-icons/im"
import Title from "../elements/Title"
import Input from "../elements/Input"
import Button from "../elements/Button"


const ModalLogin = ({ close }) => {

	const [data, setData] = useState({
		username: "",
		password: ""
	})

	const inputsList = [
		{
			name: "Nom d'utilisateur",
			varName: "username",
			value: data.username,
			type: "text",
			items: null,
		},
		{
			name: "Mot de passe",
			varName: "password",
			value: data.password,
			type: "text",
			items: null,
		}
	]

	const handleChange = (e) => {
		const name = e.target.name;
		setData({ ...data, [name]: e.target.value });
	}

	return (
		<section className="bg-transparent w-full min-h-full flex justify-center items-center ">
			<div className="lg:w-[50vw] md:w-[70vw] w-[100vw] bg-white py-4 px-5 relative m-4 rounded-sm ">
				<ImCross
					className="text-primary absolute right-3 top-3 hover:cursor-pointer hover:text-blue-800"
					onClick={() => close()}
				/>
				<Title
					titleName="Accès au compte"
					subtitleName="Veuillez entrer vos identifiants pour accéder à votre compte"
				/>
				<div className="justify-center items-center mb-14 mt-5">
					{inputsList.map((input, id) => {
						return (
							<div key={id} className="w-full my-5">
								<Input
									type={input.type}
									name={input.name}
									varName={input.varName}
									value={input.value}
									handleChange={handleChange}
								/>
							</div>
						)
					})}
				</div>
				<Button
					name="Accéder au compte"
					filled={true}
					handleClick={() => {
						console.log(data)
						close()
					}}
				/>
			</div>
		</section>
	)
}

export default ModalLogin
