import React from "react";
import { ImCross } from "react-icons/im";
import Title from "./../elements/Title";
import Button from "./../elements/Button";
import PersonalInfoInput from "./PersonalInfoInput";

const CreateMedicalFilePopup = ({ close }) => {
  return (
    <section className="bg-transparent w-full min-h-full flex justify-center items-center ">
      <div className="lg:w-[50vw] md:w-[70vw] w-[100vw] bg-white py-4 px-5 relative m-4 rounded-sm ">
        <ImCross
          className="text-primary absolute right-3 top-3 hover:cursor-pointer hover:text-blue-800"
          onClick={() => close()}
        />
        <Title
          titleName="Ajout d'un nouveau dossier"
          subtitleName="Créez un nouveau dossier patient en remplissant les champs suivants"
        />
        <PersonalInfoInput itemsPerLine={2} modify={false} disabled={false} />
        <div className="my-2">
          <Button
            name="Créer un nouveau compte patient"
            filled={true}
            handleClick={() => {
              console.log("quelquechsoe");
              close();
            }}
          />
        </div>
      </div>
    </section>
  );
};

export default CreateMedicalFilePopup;
