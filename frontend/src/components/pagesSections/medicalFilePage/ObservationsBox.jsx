import React, { useState } from "react";
import { Modal } from "@mui/material";
import { BsPlusLg } from "react-icons/bs";
import { ImCross } from "react-icons/im";
import { AiFillSave } from "react-icons/ai";
import Button from "../../elements/Button";
import AddInfoModal from "./AddInfoModal";

const ObservationsBox = ({ data,isPatient }) => {
  const [observationsList, setObservationsList] = useState(data);
  const [disabled, setDisabled] = useState(true);
  const [openModal, setOpenModal] = useState(false);

  const handleClose = () => setOpenModal(false);
  const handleOpen = () => setOpenModal(true);

  const addData = (content) => {
    let date = new Date();
    setObservationsList([
      ...observationsList,
      {
        content: content,
        date: `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`,
      },
    ]);
    handleClose();
  };

  return (
    <div className="relative bg-white  min-h-[12vh] md:min-h-[20vh] border rounded-md md:py-4">
      <div className="flex  flex-wrap absolute left-[20%] top-[-20px] md:left-0">
        <div className=" flex justify-between items-center bg-secondary border rounded-full text-primary  text-sm md:text-xl py-1 md:py-2 px-3 md:px-5 ">
          <p>Observations</p>
          <div
            className={` bg-primary text-secondary text-sm border rounded-full p-1 ml-2 hover:cursor-pointer ${
              (!disabled || isPatient) && "hidden"
            }`}
            onClick={() => {
              handleOpen();
              setDisabled(false);
            }}
          >
            <BsPlusLg />
          </div>
        </div>
      </div>

      <div className="max-h-[58vh] overflow-y-auto">
        {" "}
        <div className="my-5 mx-5">
          {observationsList.map((observation, id) => {
            return (
              <div key={id} className="bg-slate-100 p-3 my-3 border rounded-md">
                <p className="text-sm md:text-xl">{observation.content}</p>

                <div className="flex justify-end ">
                  <p>{observation.date}</p>
                </div>
              </div>
            );
          })}

          {observationsList.length === 0 && (
            <div className="text-center font-bold text-md md:text-2xl text-slate-400">
              <p>Aucune ordonnance pour le moment</p>
            </div>
          )}
        </div>
        <div
          className={`${
            !disabled ? "flex justify-center items-center [&>*]:mx-2" : "hidden"
          }`}
        >
          <Button
            name="Annuler"
            filled={false}
            color={"#FF0000"}
            handleClick={() => {observationsList.pop(); setDisabled(true)}}
          >
            <ImCross />
          </Button>
          <Button
            name="Enregistrer"
            filled={true}
            handleClick={() => setDisabled(true)}
          >
            <AiFillSave />
          </Button>
        </div>
      </div>
      <div className="min-h-full">
        <Modal
          open={openModal}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <div className="w-full ">
            <AddInfoModal
              title="observation"
              close={() => {
                handleClose();
                setDisabled(true);
              }}
              handleClick={addData}
            />
          </div>
        </Modal>
      </div>
    </div>
  );
};

export default ObservationsBox;
