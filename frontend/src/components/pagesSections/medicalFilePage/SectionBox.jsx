import React from "react";
import ExamsBox from "./ExamsBox";
import OrdonnancesBox from "./OrdonnancesBox";
import PersonalInformationBox from "./PersonalInformationBox";
import ObservationsBox from "./ObservationsBox";

const SectionBox = ({ data, isPatient }) => {
  return (
    <section className="py-10 md:py-7 [&>*]:mb-10">
      <PersonalInformationBox data={data} isPatient={isPatient}/>
      <ObservationsBox data={data.observations}  isPatient={isPatient}/>
      <div className="lg:flex ">
        <div className="lg:basis-1/2 lg:mr-2 mb-10 lg:mb-none">
          <ExamsBox data={data.exams} isPatient={isPatient} />
        </div>
        <div className=" lg:basis-1/2 lg:ml-2">
          <OrdonnancesBox data={data.ordonnances} isPatient={isPatient}/>
        </div>
      </div>
    </section>
  );
};

export default SectionBox;
