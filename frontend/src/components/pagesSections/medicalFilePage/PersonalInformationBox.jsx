import React, { useState } from "react";
import PersonalInfoInput from "../PersonalInfoInput";
import { BsPencilFill } from "react-icons/bs";
import { ImCross } from "react-icons/im";
import { AiFillSave } from "react-icons/ai";
import Button from "../../elements/Button";

const PersonalInformationBox = ({ data, isPatient }) => {
  const [disabled, setDisabled] = useState(true);

  return (
    <div className="relative bg-white min-h-[20vh] border rounded-md md:py-4">
      <div className="flex  flex-wrap absolute left-[20%] top-[-20px] md:left-0">
        <div className=" flex justify-between items-center bg-secondary border rounded-full text-primary  text-sm md:text-xl py-1 md:py-2 px-3 md:px-5 ">
          <p>Infomations personnelles</p>
          <div
            className={` bg-primary text-secondary text-sm border rounded-full p-1 ml-2 hover:cursor-pointer ${
              (!disabled || isPatient)  && "hidden"
            }`}
            onClick={() => setDisabled(false)}
          >
            <BsPencilFill />
          </div>
        </div>
      </div>

      <div className="my-10 mx-5 ">
        <PersonalInfoInput
          itemsPerLine={3}
          modify={true}
          data={data}
          disabled={disabled}
        />
      </div>
      <div className={`${!disabled ? "flex justify-center items-center [&>*]:mx-2": "hidden"}`}>
        <Button name="Annuler" filled={false} color={"#FF0000"} handleClick={() => setDisabled(true)}>
          <ImCross />
        </Button>
        <Button name="Enregistrer" filled={true} handleClick={() => setDisabled(true)}>
          <AiFillSave />
        </Button>
      </div>
    </div>
  );
};

export default PersonalInformationBox;
