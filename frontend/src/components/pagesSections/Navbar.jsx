import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { IoIosAddCircle } from 'react-icons/io'
import { Modal } from "@mui/material"
import ModalPopup from '../pagesSections/CreateMedicalFilePopup'
import ModalLogin from './ModalLogin'
import logo from '../../media/logo.png'


function Navbar({page}) {

    const [openModal, setOpenModal] = useState(false)
    const handleClose = () => setOpenModal(false)
	const handleOpen = () => setOpenModal(true)

    const connexion = () => {
        return <>
                <div className='my-auto'>
                    <div onClick={() => handleOpen()} className="flex justify-center px-5 py-2 bg-primary text-white border rounded-full hover:cursor-pointer">
                        <div>
                            <p>Connexion</p>
                        </div>
                    </div>
                </div>
                <div className="min-h-full">
                    <Modal
                        open={openModal}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                        sx={{ overflowY: "scroll" }}
                    >
                        <div className="w-full">
                            <ModalLogin close={handleClose} />
                        </div>
                    </Modal>
                </div>
        </>
    }

    const addMedicalFile = () => {
        return <>
                <div className='my-auto'>
                    <div onClick={() => handleOpen()} className="flex px-4 py-2 bg-primary text-white border rounded-full hover:cursor-pointer">
                        <IoIosAddCircle color='white' size={24} className='mr-2' />
                        <p>Ajouter un dossier</p>
                    </div>
                </div>
                <div className="min-h-full">
                    <Modal
                        open={openModal}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                        sx={{ overflowY: "scroll" }}
                    >
                        <div className="w-full">
                            <ModalPopup close={handleClose} />
                        </div>
                    </Modal>
                </div>
        </>
    }

    const createFile = () => {
        return(
            <Link to='/' className='my-auto'>
                <p className='text-red-500 underline'>Deconnexion</p>
            </Link>
        )
    }

    return (
        <div className='sticky top-0 z-30 w-full flex justify-between lg:px-10 px-3 bg-gray-50'>
            <Link to='/'>
                <div className='flex'>
                    <img src={logo} alt="" width={70} height={70} />
                    <p className='text-blue-900 font-bold my-auto'>Dossier Medical</p>
                </div>
            </Link>
            <div className='flex'>
                { 
                    page === 'search' ?
                        addMedicalFile()
                    : null
                }
                {
                    page === 'home' ?
                        connexion()
                    : null
                }
                {
                    page === 'file' ?
                        createFile()
                    : null
                }
            </div>
        </div>
    )
}

export default Navbar