import React from "react";

const Title = ({ titleName, subtitleName }) => {
  return (
    <div className="text-center py-3">
      <h5 className="text-primary text-xl md:text-2xl font-bold">{titleName}</h5>
      {subtitleName && <p className="text-slate-900 text-md md:text-xl py-2">{subtitleName}</p>}
    </div>
  );
};

export default Title;
