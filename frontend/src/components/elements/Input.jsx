import React from "react";

const Input = ({ name,varName, value,type, handleChange,placeholder, disabled }) => {
  return (
    <div className="w-full">
      <p className="text-primary text-md md:text-xl">{name}</p>
      <input
        type= {type}
        disabled={disabled}
        className="peer w-full px-2 py-2 border border-slate-300 rounded-md text-[18px] shadow-sm placeholder-slate-400 focus:outline-none focus:border-primary focus:border-2 "
        placeholder={placeholder && placeholder}
        name={varName}
        value={value}
        onChange={(e) => handleChange(e)}
      />
    </div>
  );
};

export default Input;
