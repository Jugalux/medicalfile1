import React from 'react'
import IconButton from "@mui/material/IconButton"
import TextField from "@mui/material/TextField"
import { FiSearch } from 'react-icons/fi'

function SearchBar({setSearchQuery}) {
    return (
        <form className='flex'>
            <TextField
                id="search-bar"
                className="text w-full bg-white"
                onInput={(e) => {
                    setSearchQuery(e.target.value);
                }}
                color='primary'
                label="Entrez un nom ou un code patient..."
                variant="filled"
                placeholder="Rechercher..."
                size="small"
            />
            <div className='bg-blue-900 rounded-r'>
                <IconButton type="submit" aria-label="search">
                    <FiSearch color='#fff' />
                    <p className='text-lg mx-3 text-white font-bold'>Rechercher</p>
                </IconButton>
            </div>
        </form>
    )
}

export default SearchBar