import React from "react";

const SelectInput = ({ name,varName,selectedValue, handleChange, listValues, disabled }) => {
  return (
    <div className="w-full">
        <p className="text-primary text-md md:text-xl">{name}</p>
      <select
      name={varName}
        value={selectedValue}
        disabled={disabled}
        onChange={(e) => handleChange(e)}
        className="peer w-full px-2 py-[10px] border border-slate-300 rounded-md text-[18px] shadow-sm placeholder-slate-400 focus:outline-none focus:border-primary focus:border-2 "
      >
        {listValues.map((value, id) => {
          return (
            <option key={id} value={value}>
              {value}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default SelectInput;
